FROM rocker/geospatial:4.0.1
# RUN apt-get update && apt-get install -y  gdal-bin git-core libcairo2-dev libcurl4-openssl-dev libgdal-dev libgeos-dev libgeos++-dev libgit2-dev libicu-dev libpng-dev libpq-dev libproj-dev libssl-dev libudunits2-dev libxml2-dev make pandoc pandoc-citeproc zlib1g-dev && rm -rf /var/lib/apt/lists/*
RUN apt-get update -qq && apt-get install -y libharfbuzz-dev libfribidi-dev
RUN echo "options(repos = c(CRAN = 'https://cran.rstudio.com/'), download.file.method = 'libcurl', Ncpus = 4)" >> /usr/local/lib/R/etc/Rprofile.site
RUN R -e 'install.packages("remotes")'
RUN R -e 'install.packages("renv")'
# RUN Rscript -e 'remotes::install_version("stringr",upgrade="never", version = "1.4.0")'
# RUN Rscript -e 'remotes::install_version("glue",upgrade="never", version = "1.4.2")'
# RUN Rscript -e 'remotes::install_version("magrittr",upgrade="never", version = "2.0.1")'
# RUN Rscript -e 'remotes::install_version("withr",upgrade="never", version = "2.4.2")'
# RUN Rscript -e 'remotes::install_version("rmarkdown",upgrade="never", version = "2.7")'
# RUN Rscript -e 'remotes::install_version("knitr",upgrade="never", version = "1.33")'
# RUN Rscript -e 'remotes::install_version("fs",upgrade="never", version = "1.5.0")'
# RUN Rscript -e 'remotes::install_version("purrr",upgrade="never", version = "0.3.4")'
# RUN Rscript -e 'remotes::install_version("dplyr",upgrade="never", version = "1.0.7")'
# RUN Rscript -e 'remotes::install_version("tidyr",upgrade="never", version = "1.1.2")'
# RUN Rscript -e 'remotes::install_version("readr",upgrade="never", version = "1.3.1")'
# RUN Rscript -e 'remotes::install_version("forcats",upgrade="never", version = "0.5.0")'
# RUN Rscript -e 'remotes::install_version("data.table",upgrade="never", version = "1.13.0")'
# RUN Rscript -e 'remotes::install_version("DBI",upgrade="never", version = "1.1.1")'
# RUN Rscript -e 'remotes::install_version("assertthat",upgrade="never", version = "0.2.1")'
# RUN Rscript -e 'remotes::install_version("promises",upgrade="never", version = "1.1.1")'
# RUN Rscript -e 'remotes::install_version("ggplot2",upgrade="never", version = "3.3.2")'
# RUN Rscript -e 'remotes::install_version("pkgload",upgrade="never", version = "1.2.1")'
# RUN Rscript -e 'remotes::install_version("usethis",upgrade="never", version = "2.0.1")'
# RUN Rscript -e 'remotes::install_version("shiny",upgrade="never", version = "1.6.0")'
# RUN Rscript -e 'remotes::install_version("testthat",upgrade="never", version = "3.0.2")'
# RUN Rscript -e 'remotes::install_version("here",upgrade="never", version = "1.0.1")'
# RUN Rscript -e 'remotes::install_version("config",upgrade="never", version = "0.3")'
# RUN Rscript -e 'remotes::install_version("sf",upgrade="never", version = "0.9-7")'
# RUN Rscript -e 'remotes::install_version("tidyverse",upgrade="never", version = "1.3.0")'
# RUN Rscript -e 'remotes::install_version("shinythemes",upgrade="never", version = "1.1.2")'
# RUN Rscript -e 'remotes::install_version("shinipsum",upgrade="never", version = "0.1.0")'
# RUN Rscript -e 'remotes::install_version("santoku",upgrade="never", version = "0.5.0")'
# RUN Rscript -e 'remotes::install_version("RPostgres",upgrade="never", version = "1.2.1")'
# RUN Rscript -e 'remotes::install_version("rcmdcheck",upgrade="never", version = "1.3.3")'
# RUN Rscript -e 'remotes::install_version("golem",upgrade="never", version = "0.3.1")'
# RUN Rscript -e 'remotes::install_version("ggspatial",upgrade="never", version = "1.1.5")'
# RUN Rscript -e 'remotes::install_version("ggrepel",upgrade="never", version = "0.9.1")'
# RUN Rscript -e 'remotes::install_version("ggiraph",upgrade="never", version = "0.7.8")'
# RUN Rscript -e 'remotes::install_version("future",upgrade="never", version = "1.21.0")'
# RUN Rscript -e 'remotes::install_version("cartography",upgrade="never", version = "2.4.2")'
# RUN Rscript -e 'remotes::install_github("MaelTheuliere/COGiter@b620601c788b027dc763dbcb493ee6f80595fc0f")'
RUN mkdir /build_zone
ADD . /build_zone
WORKDIR /build_zone
RUN R -e 'renv::restore();remotes::install_local(upgrade="never");renv::snapshot()'
# RUN rm -rf /build_zone
EXPOSE 3838
CMD  ["R", "-e", "options('shiny.port'=3838,shiny.host='0.0.0.0');options(golem.app.prod = FALSE);propre.itdd::run_app()"]
