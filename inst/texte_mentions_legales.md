<center>
# **Mentions légales**
</center>
<hr>

<table>
<td width="5%"></td> <!-- espace entre colonne -->

<td width=20% valign=top>
<h4><b>Service gestionnaire</b></h4>
<b>Direction de l’Environnement, de l’Aménagement<br>
et du Logement de La Réunion</b><br>
2, rue Juliette Dodu - CS 41009<br>
97743 Saint-Denis cedex 9
<hr>
<h4><b>Contacts</h4></b>
<b>DEAL Réunion – Service SCETE</b><br>
Unité Connaissance et Prospective<br>
Tél : 02 62 40 26 47<br>
Mel : <a href="mailto:statistiques.deal-reunion@developpement-durable.gouv.fr">statistiques.deal-reunion@developpement-durable.gouv.fr</a>
<hr>
<h4><b>Directeur de la publication</h4></b>
<b>Philippe GRAMMONT</b>,<br> 
Directeur de la Direction de l’Environnement,<br>
de l’Aménagement et du Logement<br>
DEAL Réunion
<hr>
<h4><b>Conception, réalisation, développement</h4></b>
<b>Contenu éditorial, charte graphique, ergonomie</b>
<li>réseau des statisticiens en DREAL/DEAL/DRIEAT<br>
<b>Développement</b>
<li>Caroline Coudrin, DEAL Réunion
<li>ThinkR - 5O rue Arthur Rimbaud - 93300 Aubervilliers 
</td>

<td width="5%"></td> <!-- espace entre colonne -->

<td width=30% valign=top>
<div align="justify";>
<h4><b>Droit d’auteur – Licence</h4></b>
Tous les contenus présents sur ce site sont couverts par le droit d’auteur. 
Toute reprise est dès lors conditionnée à l’accord de l’auteur en vertu de l’article L.122-4 du Code de la Propriété Intellectuelle.

Toutes les informations liées à cette application (données et textes) sont publiées sous licence ouverte/open licence v2 (dite licence Etalab) : quiconque est libre de réutiliser ces informations sous réserve, notamment, d’en mentionner la filiation.

Tous les scripts sources de l’application sont disponibles sous licence GPL-v3.
<hr>
<h4><b>Code source</h4></b>
L’ensemble des scripts de collecte et de datavisualisation est disponible sur le <a href=https://gitlab.com/rdes_dreal/propre.itdd target="_blank"> répertoire gitlab du réseau des statisticiens en DREAL </a>. Vous pouvez y reporter les éventuels bugs ou demandes d’évolution au niveau de la rubrique Issues.
<hr>
<h4><b>Établir un lien</h4></b>
<li>Tout site public ou privé est autorisé à établir, sans autorisation préalable, un lien vers les informations diffusées par le Ministère de la Transition Ecologique et par le Ministère de la Cohésion des Territoires et des Relations avec les Collectivités Territoriales et par la DEAL Réunion en particulier.<br><br>
<li>L’autorisation de mise en place d’un lien est valable pour tout support, à l’exception de ceux diffusant des informations à caractère polémique, pornographique, xénophobe ou pouvant, dans une plus large mesure porter atteinte à la sensibilité du plus grand nombre.<br><br>
<li>Pour ce faire, et toujours dans le respect des droits de leurs auteurs, une icône Marianne est disponible sur le <a href=https://www.gouvernement.fr/marque-Etat target="_blank"> site de la marque de l'État </a> pour agrémenter votre lien et préciser que le site d’origine est celui de la DEAL Réunion.

<td width="5%"></td> <!-- espace entre colonne -->

<td width=30% valign=top>
<div align="justify";>
<h4><b>Usage</h4></b>
<li>Les utilisateurs sont responsables des interrogations qu’ils formulent ainsi que de l’interprétation et de l’utilisation qu’ils font des résultats. Il leur appartient d’en faire un usage conforme aux réglementations en vigueur et aux recommandations de la CNIL lorsque des données ont un caractère nominatif (loi n° 78.17 du 6 janvier 1978, relative à l’informatique, aux fichiers et aux libertés dite loi informatique et libertés).
<br><br>
<li>Il appartient à l’utilisateur de ce site de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination par d’éventuels virus circulant sur le réseau Internet. De manière générale, la DEAL Réunion décline toute responsabilité quant à un éventuel dommage survenu pendant la consultation du présent site. Les messages que vous pouvez nous adresser transitant par un réseau ouvert de télécommunications, nous ne pouvons assurer leur confidentialité.
<br><br>
<li>En utilisant le site, vous reconnaissez avoir pris connaissance de ces conditions et les avoir acceptées. Celles-ci pourront être modifiées à tout moment et sans préavis par la DEAL Réunion.
<hr>
<h4><b>Données personnelles</h4></b>
L’outil ne fait pas d’usage interne de données à caractère personnel. 
<hr>
<h4><b>Cookies</h4></b>Pour des besoins de statistiques et d’affichage, le présent site peut utiliser des cookies. Il s’agit de petits fichiers textes stockés sur votre disque dur afin d’enregistrer des données techniques sur votre navigation. Certaines parties de ce site ne peuvent être fonctionnelles sans l’acceptation de cookies.
</div>

</td>
<td width="5%"></td> <!-- espace entre colonne -->
</table>
<hr>
<center><h5><b>Hébergement : </b><a href=https://datalab.sspcloud.fr/home target="_blank">Plateforme SSPCloud Insee</a></h5></center>