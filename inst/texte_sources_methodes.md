<center>
# **Pour comprendre les indicateurs**
</center>
<hr>

<table>
<td width="2%"></td> <!-- espace entre colonne -->

<td width=30% valign=top>
<h4><b>Sources</b></h4>
Les indicateurs territoriaux de développement durable qui font l’objet d’ODDetT sont mis à disposition sur le <a href=https://www.statistiques.developpement-durable.gouv.fr/catalogue?page=dataset&datasetId=632956d8eae137714f60ae22 target="_blank">site du SDES</a> et sur le <a href=https://www.insee.fr/fr/statistiques/4505239 target="_blank">site de l’Insee</a>; ils sont proposés dans des fichiers en téléchargement permettant une approche régionale, départementale et communale. Quand cela est possible, plusieurs millésimes de données sont inclus.
<br><br>
Ces indicateurs font l’objet de <a href=https://www.insee.fr/fr/statistiques/fichier/4505239/informations_112_indicateurs.zip target="_blank"> fiches descriptives </a> très complètes, accessibles au même endroit.
<br><br>
L’ensemble de ces indicateurs (appelé kit Antidote SL26) est actualisé tous les ans. Le millésime du kit figure dans la source de chacune des illustrations de ODDetT. Les indicateurs livrés sont amenés à évoluer chaque année.
<br><br>
La liste de l’ensemble des indicateurs du kit Antidote est disponible <a href=https://www.insee.fr/fr/statistiques/4505239#dictionnaire target="_blank"> ici </a>.
<br><br>
<br><br>
<h4><b>Méthodes</b></h4>
Dans les cartes affichées, les seuils sont créés automatiquement en séparant en 5 groupes les données du niveau géographique représenté. En conséquence, les seuils sont personnalisés et différents d'une carte à l'autre.

<td width="2%"></td> <!-- espace entre colonne -->

<td width=30% valign=top>
<h4><b>Définitions</b></h4>
Pour une meilleure compréhension des représentations graphiques, voici quelques définitions : 
<br><br>
<li>La <b>médiane</b> est la valeur qui sépare un ensemble en deux parties égales, autant en dessous, autant au-dessus. Dans les graphiques disponibles, cet ensemble dépend du niveau géographique représenté.
<br><br>
<li><b>Acoss</b> : Agence centrale des organismes de sécurité sociale.
<li><b>Ademe</b> : Agence De l'Environnement et de la Maîtrise de l'Énergie.
<li><b>AFB</b> : Agence française pour la biodiversité.
<li><b>Agreste</b> : La statistique, l'évaluation et la prospective agricole du ministère de l'agriculture et de l'alimentation.
<li><b>ANSP</b> : Agence nationale des services à la personne.
<li><b>Arcep</b> : Autorité de régulation des communications électroniques, des postes et de la distribution de la presse.
<li><b>BRGM</b> : Bureau de recherches géologiques et minières.
<li><b>CERTU</b> : Centre d’études sur les réseaux, les transports, l’urbanisme et les constructions publiques.
<li><b>CGDD</b> : Commissariat général au développement durable.
<li><b>CITEPA</b> : Centre interprofessionnel technique d'études de la pollution atmosphérique.
<li><b>CNAM</b> : Caisse Nationale d'Assurance Maladie.

<td width="2%"></td> <!-- espace entre colonne -->

<td width=30% valign=top>

<li><b>DEPP</b> : Direction de l'évaluation, de la prospective et de la performance.
<li><b>DGALN</b> : Direction générale de l'Aménagement, du Logement et de la Nature, MTES.
<li><b>DGESIP</b> : Direction générale de l'enseignement supérieur et de l'insertion professionnelle, MESR.
<li><b>DGPR</b> : La direction générale de la Prévention des risques, MTES.
<li><b>DGRI</b> : Direction générale de la recherche et de l'innovation, MESR.
<li><b>DREES</b> : Direction de la Recherche, des Études, de l'Évaluation et des Statistiques.
<li><b>DSN</b> : Déclaration sociale nominative.
<li><b>IFRECOR</b> : Initiative française pour les récifs coralliens.
<li><b>INPES</b> : Institut national de prévention et d'éducation pour la santé.
<li><b>Insee</b> : Institut national de la statistique et des études économiques.
<li><b>Inserm</b> : Institut national de la santé et de la recherche médicale.
<li><b>MENJVA</b> : Ministère de l'éducation nationale, de la jeunesse et de la vie associative.
<li><b>MESR</b> : Ministere de l'enseignement superieur et de la recherche.
<li><b>MTES</b> : Ministère de la Transition Écologique et Solidaire.
<li><b>Onema</b> : Office national de l'eau et des milieux aquatiques.
<li><b>ONISR</b> : Observatoire national interministériel de la sécurité routière.
<li><b>SDES</b> : Service des données et études statistiques des ministères chargés de l'environnement, de l'énergie, de la construction, du logement et des transports (anciennement SoeS).
<li><b>SIES</b> : Sous-direction des systèmes d'information et des études statistiques, à la DGRI, MESR.
<li><b>SSP</b> : Service de la statistique et de la prospective qui est un service du Secrétariat Général du Ministère de l'agriculture.
<li><b>Unicem</b> : Union nationale des industries de carrières et matériaux de construction.

</td>
<td width="2%"></td> <!-- espace entre colonne -->
</table>