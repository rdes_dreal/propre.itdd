<center>
# **L’agenda 2030 et les ODD**
</center>
<hr>

<table>
<td width="2%"></td> <!-- espace entre colonne -->

<td width=33% valign=top>
<center><img src="objectifs_de_DD.png" alt="" style="max-width: 100%;"></center>
<br>
<div align="justify";>En septembre 2015, les 193 États membres de l’ONU ont adopté le programme de développement durable à l’horizon 2030 intitulé <b>Agenda 2030</b>. C'est un agenda pour les populations, pour la planète, pour la prospérité, pour la paix et mis en œuvre à travers des partenariats. Il porte une vision de transformation de notre monde en éradiquant la pauvreté et en assurant sa transition vers un développement durable.

<center><img src="les_5_P.png" alt="" style="max-width: 100%;"></center>

L’Agenda 2030 a fusionné l'agenda du développement et celui des Sommets de la Terre. Il est <b>universel</b>, c’est-à-dire qu'il s’applique à tous les pays, du Nord comme du Sud. À cet égard, tous les pays sont considéres comme «<b>en voie de développement durable</b>». 
<br>
Avec ses <b>17 Objectifs de développement durable</b> et <b>169 cibles</b> (ou sous-objectifs), il dessine une feuille de route détaillée et couvre pratiquement toutes les questions de société.
<br><br>
Pour plus de renseignements sur l’<a href=https://www.agenda-2030.fr target="_blank"><b>agenda 2030</b></a> et <b>les ODD</b> : 
<li><a href=https://www.agenda-2030.fr/agenda2030/presentation-principes-specificites-origines-18 target="_blank">Présentation : origines et principes</a>
<li><a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/ target="_blank">Les 17 objectifs de développement durable</a>
<li><a href=https://www.agenda-2030.fr/agenda2030/situation-de-la-france-21 target="_blank">Situation et organisation de la mise en œuvre en France</a>
<li><a href=https://www.agenda-2030.fr/agenda2030/mobilisation-des-acteurs-non-etatiques-en-france-40 target="_blank">Mobilisation des acteurs</a>
<li><a href=https://www.agenda-2030.fr/agenda2030/en-europe-et-linternational-22 target="_blank">En Europe et à l'international</a>
</div>

<td width="5%"></td> <!-- espace entre colonne -->

<td width=33% valign=top>
<h4><b>Pour mieux connaître les ODD</b>, rendez-vous également sur le site <a href=https://www.methodd.fr target="_blank"><img src="la_method_odd.png" alt="" style="max-width: 25%;"></a></h4>
<div align="justify";>Le site <b>"La Méth’ODD"</b> vise à faciliter l’appropriation et l’intégration des ODD dans les projets de territoire. Il s’agit d’une méthode d’accompagnement des acteurs des territoires, qui propose des repères et des outils pratiques pour s’emparer des ODD. Six parcours utilisateurs sont proposés avec les outils d’animation associés. Vous y trouverez également d’autres initiatives régionales : <a href=https://www.methodd.fr/blog-des-regions/ target="_blank">Le blog des régions</a>.
<hr>
<h4><b>Et sur les indicateurs de développement durable</b>, vous pouvez vous rendre sur :</h4>
<li><a href=https://www.agenda-2030.fr/agenda2030/dispositif-de-suivi-les-indicateurs-19 target="_blank">Les indicateurs de suivi des Objectifs de développement durable </a> (agenda-2030.fr)
<li><a href=https://www.insee.fr/fr/statistiques/2654964 target="_blank">Les indicateurs pour le suivi national des objectifs de développement durable </a> (insee.fr)

<h4>...autres liens vers des indicateurs régionaux :</h4>  
<li><a href=https://www.reunion.developpement-durable.gouv.fr/objectifs-et-indicateurs-du-developpement-durable-r47.html target="_blank">A La Réunion</a>  (DEAL de La Réunion)
<li><a href=http://www.centre-val-de-loire.developpement-durable.gouv.fr/les-odd-dans-mon-territoire-quiz-et-portraits-a3639.html target="_blank">En Centre Val de Loire</a> et <a href=http://www.centre-val-de-loire.developpement-durable.gouv.fr/les-odd-en-videos-des-initiatives-en-region-centre-a3557.html target="_blank">les ODD en vidéos</a></h4></b> (DREAL Centre-Val-de-Loire)
<li><a href=http://www.nouvelle-aquitaine.developpement-durable.gouv.fr/objectifs-de-developpement-durable-r4223.html target="_blank">En Nouvelle Aquitaine</a> (DREAL Nouvelle-Aquitaine)
<li><a href=https://www.insee.fr/fr/statistiques/5358969 target="_blank">En Occitanie</a> (Insee) 
<li><a href=https://ssm-ecologie.shinyapps.io/observatoire-odd/accueil target="_blank">En Île-de-France</a> (DRIEAT) 
</div>
<a href=https://i.imgur.com/VyyH8P8.png target="_blank"><div align="right"><img src="Elyx_Yak_100.png" alt="" style="max-width: 58%;"></div>
</a>

</td>
<td width="2%"></td> <!-- espace entre colonne -->
</table>
