<table>
<td width="15%">
<td width="70%" align="center"><center><img src="www/sigle_ODDetT.png" alt="" style="max-width: 20%;"></center>
<td width="15%" align="center"><a href=https://www.reunion.developpement-durable.gouv.fr/IMG/pdf/oddettguidutil.pdf target="_blank"><img src="bouton_guide_util2.png" alt="" style="max-width: 100%;"></a>
</td></table>
<center><h1><b>l’outil national de visualisation des indicateurs de développement durable</b></h1></center>
<table>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd1-eliminer-la-pauvrete-sous-toutes-ses-formes-et-partout-dans-le-monde target="_blank"><img src="ODD_1.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd2-eliminer-la-faim-assurer-la-securite-alimentaire-ameliorer-la-nutrition-et target="_blank"><img src="ODD_2.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd3-donner-aux-individus-les-moyens-de-vivre-une-vie-saine-et-promouvoir-le target="_blank"><img src="ODD_3.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd4-veiller-a-ce-que-tous-puissent-suivre-une-education-de-qualite-dans-des target="_blank"><img src="ODD_4.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd5-realiser-l-egalite-des-sexes-et-autonomiser-toutes-les-femmes-et-les target="_blank"><img src="ODD_5.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd6-garantir-l-acces-de-tous-a-l-eau-et-a-l-assainissement-et-assurer-une target="_blank"><img src="ODD_6.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd7-garantir-l-acces-de-tous-a-des-services-energetiques-fiables-durables-et target="_blank"><img src="ODD_7.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd8-promouvoir-une-croissance-economique-soutenue-partagee-et-durable-le-plein target="_blank"><img src="ODD_8.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd9-mettre-en-place-une-infrastructure-resiliente-promouvoir-une target="_blank"><img src="ODD_9.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd10-reduire-les-inegalites-entre-les-pays-et-en-leur-sein target="_blank"><img src="ODD_10.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd11-faire-en-sorte-que-les-villes-et-les-etablissements-humains-soient target="_blank"><img src="ODD_11.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd12-etablir-des-modes-de-consommation-et-de-production-durables target="_blank"><img src="ODD_12.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd13-prendre-d-urgence-des-mesures-pour-lutter-contre-les-changements target="_blank"><img src="ODD_13.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd14-conserver-et-exploiter-de-maniere-durable-les-oceans-les-mers-et-les target="_blank"><img src="ODD_14.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd15-preserver-et-restaurer-les-ecosystemes-terrestres target="_blank"><img src="ODD_15.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd16-promouvoir-l-avenement-de-societes-pacifiques-et-ouvertes-aux-fins-du target="_blank"><img src="ODD_16.png" alt="" style="max-width: 95%;"></a>
<td>
<a href=https://www.agenda-2030.fr/17-objectifs-de-developpement-durable/article/odd17-partenariats-pour-la-realisation-des-objectifs target="_blank"><img src="ODD_17.png" alt="" style="max-width: 95%;"></a>
</td></table>
<hr>


<table>
<td width="2%">
<td width="30%" valign="top">

<h3><b>C’EST QUOI ?</b></h3>
<div><h4><b>ODDetT</b> est une initiative de la DEAL Réunion qui permet, pour tous les territoires métropolitains et ultramarins, de <b>visualiser des représentations des indicateurs du développement durable</b>. Vous pouvez accéder à ces résultats sur votre territoire et sur les territoires voisins, et vous comparer à d’autres niveaux géographiques.
<br><br>
Cette application a été développée par le réseau des directions régionales de l’environnement de l’aménagement et du logement (DREAL, DEAL et DRIEAT), en partenariat avec l’Insee et le CGDD.
<br><br>Elle s’appuie sur une <a href=https://www.statistiques.developpement-durable.gouv.fr/catalogue?page=dataset&datasetId=632956d8eae137714f60ae22 target="_blank"><b>palette d’indicateurs</b></a> éclairant les ODD au niveau national et territorial (régions, départements, intercommunalités et communes) transmise par l’Insee et le Service des données et des études statistiques (SDES) du ministère de la Transition écologique (kit Antidote).
</div>
<font size='2'><a href=https://www.insee.fr/fr/statistiques/fichier/4505239/dictionnaire_indicateurs_territoriaux_developpement_durable.xlsx target="_blank"><img src="telecharger.png" alt=""><b> Télécharger le dictionnaire de variables des indicateurs</b></a></font>
</td>

<td width="3%">
</td>

<td width=30% valign="top">

<h3><b>POUR QUOI FAIRE ?</b></h3>
<h4><b><li>Visualiser</b> des représentations des indicateurs du développement durable,
<hr>
<b><li>Trouver</b> des repères chiffrés et se comparer à d’autres niveaux géographiques,
<hr>
<b><li>Suivre</b> les contributions à l’atteinte des objectifs de développement durable.</h4>
<hr>
</td>

<td width="3%">
</td>

<td width=30% valign="top">

<h3><b>POUR QUI ?</b></h3>
<h4><b>ODDetT s’adresse à un large public</b> car les ODD concernent par définition tous les acteurs :
<br><br>
<li> Collectivités territoriales (élus et techniciens),
<br><br>
<li> Établissements publics,
<br><br>
<li> Établissement d’enseignement et de recherche,
<br><br>
<li> Services de l’État,
<br><br>
<li> Entreprises,
<br><br>
<li> Associations,
<br><br>
<li> Citoyens etc…
</h4>

</td>
<td width="2%">
</td>
</table>