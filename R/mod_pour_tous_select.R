#' pour_tous_select UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#'
#' @importFrom shiny NS tagList 
#' @importFrom dplyr filter
#' @importFrom stats setNames
mod_pour_tous_select_ui <- function(id){
  ns <- NS(id)
  tagList(
    h6("Tous les champs des liste d\u00E9roulante peuvent faire l'objet de saisie de texte pour faire une recherche. Il faut supprimer d'abord la valeur affich\u00E9e avec la touche de retour arri\u00E8re."),
    h3("Choisir sa g\u00E9ographie :"),
    selectInput(
      ns("niveau"),
      label ="Niveau g\u00E9ographique",
      choices = c( "National" = "metro",
                   "R\u00E9gions" = "region",
                   "D\u00E9partements m\u00E9tropolitains" = "departement",
                   "D\u00E9partements Outre Mer" = "dom",
                   "EPCI" = "epci", 
                   "Communes m\u00E9tropolitaines" = "commune",
                   "Communes dans un DOM" = "comdom")
    ),
    selectizeInput(
      ns("geo"), 
      label = "Territoire", 
      choices = NULL
    ),
    h3("Choisir l'ODD :"),
    selectizeInput(
      ns("odd"), 
      label = "Liste des ODD", 
      choices = "Tous"
    ), 
    h3("Choisir l'indicateur :"),
    selectizeInput(
      ns("indic"), 
      label = "Liste des indicateurs (pour le niveau choisi)", 
      choices = NULL
    ),
    selectizeInput(
      ns("sous_champ"),
      label = "Sous-champ de l'indicateur",
      choices = NULL
    ),
    h3("Choisir l'ann\u00e9e :"),
    selectizeInput(
      ns("annee"), 
      label = "", 
      choices = NULL
    ),
    actionButton(ns("valider"), "Valider les choix"),
    tags$script(paste0("$(#",ns("valider"),").attr('disabled', 'disabled')"))
  )
}

#' pour_tous_select Server Functions
#'
#' @noRd 
mod_pour_tous_select_server <- function(id, global){
  moduleServer( id, function(input, output, session){
    ns <- session$ns
    local <- reactiveValues()
    
    
    observeEvent(input$niveau,{
      golem::invoke_js("disable", paste0("#", ns("valider")))
      
      if(input$niveau == "dom") {
        local$table_niveau <- global$tables %>% 
          pluck("departement")
      }else if(input$niveau == "comdom") {
        local$table_niveau <- global$tables %>% 
          pluck("commune")
      }else{
        local$table_niveau <- global$tables %>% 
          pluck(input$niveau)
      }
      
      if(input$niveau == "metro"){
        choices <- c("France m\u00E9tropolitaine" = "MET")
      } else if(input$niveau == "departement") {
        choices <- local$table_niveau  %>% 
          filter(str_sub(DEP, 1, 2) != "97") %>%
          pull(nom)
      } else if(input$niveau == "dom") {
        choices <- local$table_niveau  %>% 
          filter(str_sub(DEP, 1, 2) == "97") %>%
          pull(nom)
      } else if(input$niveau == "commune") {
        choices <- local$table_niveau  %>% 
          filter(str_sub(DEPCOM, 1, 2) != "97") %>%
          pull(nom)
      } else if(input$niveau == "comdom") {
        choices <- local$table_niveau  %>% 
          filter(str_sub(DEPCOM, 1, 2) == "97") %>%
          pull(nom)
      }else{
        choices <- local$table_niveau  %>% 
          arrange(nom) %>% 
          pull(nom)
      }
      
      updateSelectizeInput(session, "geo", choices = choices, server = TRUE)
    })
    
    observeEvent(input$geo,{
      golem::invoke_js("reable", paste0("#", ns("valider")))
    })
    
    observeEvent(input$niveau,{
      local$indic_base <- str_sub(input$niveau, 1, 3)
      
      if(input$niveau == "epci"){
        local$indic_base <- str_sub(input$niveau, 1, 4)
      }
      if(input$niveau == "dom"){
        local$indic_base <- "dep"
      }
      
      indics <- get_list_indic(global$con, local$indic_base )
      
      #certains indicateurs de la base ne sont que des composantes
      indics_lib<-get_ind_code_lib(global$dicovar,indics)
      indics<-stringr::str_extract(indics_lib,"i[:digit:]{3}[:alpha:]{0,1}")
      
      updateSelectInput(session, "indic", choices = setNames(indics,as.list(indics_lib)))
      
      #en fonction des indicateurs presents dans la base, renvoi l ODD concerne, pas forcement tous, selon le nivgeo: ex: ODD17
      odds <- get_odd(global$dicovar, indics)
      odd_lib <- get_odd_lib(global$fichier_odd, odds)
      
      odd_tous <- c("Tous",odds)
      odd_tous_lib <- c("Tous", odd_lib)
      
      # updateSelectInput(session, "odd", choices = c("Tous",odd))
      updateSelectInput(session, "odd", choices = setNames(odd_tous, as.list(odd_tous_lib)))
      
      golem::invoke_js("reable", paste0("#", ns("valider")))
      
      global$indic <- indics
      global$niveau <- input$niveau
      # message(indics)
    })
    
    
    observeEvent(input$odd,{
      req(input$odd)
      message("recalcul")
      if (input$odd != "Tous") {
        
        indics_filtre_odd <- intersect(global$indic,get_ind_odd(global$dicovar,input$odd))
        indics_lib<-get_ind_code_lib(global$dicovar,indics_filtre_odd)
        
      } else {
        indics_lib<-get_ind_code_lib(global$dicovar,global$indic)
      }
      
      indics<-stringr::str_extract(indics_lib,"i[:digit:]{3}[:alpha:]{0,1}")
      updateSelectInput(session, "indic", choices = setNames(indics,as.list(indics_lib)))
      
      golem::invoke_js("reable", paste0("#", ns("valider")))
      
    })
    
    observeEvent(c(input$indic, input$odd),{
      req(input$niveau)
      req(input$indic)
      golem::invoke_js("disable", paste0("#", ns("valider")))
      
      nom_tables <- tables_par_niveau(input$niveau, input$indic)
      
      data_terr <- tbl(global$con, nom_tables$nom_data_terr)
      
      
      ## choix sous_champ
     
      sous_champs <- data_terr %>% filter(type_var=="i") %>% distinct(sous_champ) %>% pull() %>% sort()
      sous_champs_lib <- get_ind_sschamp_lib(global$dicovar,input$indic,sous_champs)
      list_sschamp <- setNames(sous_champs,as.list(sous_champs_lib))
      
      # pour mettre le total en 1er dans la liste
      list_sschamp_hors_tot <- list_sschamp[!names(list_sschamp) %in% c("total","totale")] 
      tot1 <- list_sschamp["total"]
      tot2 <- list_sschamp["totale"]
      if (!is.na(tot1)) {
        list_sschamp <- c(tot1, list_sschamp_hors_tot)
      } 
      if (!is.na(tot2)) {
        list_sschamp <- c(tot2, list_sschamp_hors_tot)
      } 
      
      updateSelectInput(session, "sous_champ", choices = list_sschamp)
      
      ## choix année       
      var_an <- list(
        an = data_terr %>% filter(type_var=="i") %>% distinct(annee) %>% pull() %>% sort(decreasing = TRUE)
      )
      
      updateSelectInput(session, "annee", choices = var_an$an)
      
      golem::invoke_js("reable", paste0("#", ns("valider")))
      
      ### check si pas vide
      count_lines <- data_terr %>% 
        filter(type_var=="i") %>% 
        count() %>%
        pull(n)
      
      if( count_lines == 0){
        golem::invoke_js("disable", paste0("#", ns("valider"))) 
        showNotification(p("Aucune donn\u00E9e n'est disponible pour cet indicateur sur ce territoire"), duration = 3)
      }else{
        golem::invoke_js("reable", paste0("#", ns("valider"))) 
      }
      
    })
    
    observeEvent(c(input$annee, input$sous_champ),{
      
      golem::invoke_js("reable", paste0("#", ns("valider"))) 
    })
    
    observeEvent(input$valider,{
      showModal(
        modal_waiting()
      )
      
      golem::invoke_js("disable", paste0("#", ns("valider"))) 
      
      nom_tables <- tables_par_niveau(input$niveau, input$indic)
      global$nom_tables <- nom_tables
      try_tbl <- purrr::possibly(tbl, NULL)
      
      local$connect_tbls <- reactiveValues(
        data_terr = tbl(global$con, nom_tables$nom_data_terr) ,
        data_terr_ally = try_tbl(global$con, nom_tables$nom_data_terr_ally),
        data_ref = try_tbl(global$con, nom_tables$nom_data_ref),
        data_ref_ally = try_tbl(global$con, nom_tables$nom_data_ref_ally),
        data_comp = try_tbl(global$con, nom_tables$nom_data_comp),
        data_supra1 = try_tbl(global$con, nom_tables$nom_data_supra1),
        data_supra2 = try_tbl(global$con, nom_tables$nom_data_supra2) ,
        data_infra1 = try_tbl(global$con, nom_tables$nom_data_infra1),
        data_infra2 = try_tbl(global$con, nom_tables$nom_data_infra2),
        data_supra_bar = try_tbl(global$con, nom_tables$nom_data_supra_bar)
      )
      
      
      global$variable <- get_var_ind(global$dicovar,input$indic)
      
      # global$couleur_odd <- get_couleur_ind(global$dicovar,input$variable)
      global$sous_champ <- input$sous_champ
      global$annee <- input$annee
      
      if(input$niveau == "metro"){
        global$terr_choisi <- "MET"
      }else{
        global$terr_choisi <- local$table_niveau %>% 
          filter(nom == input$geo) %>% 
          ## attention, il faut mieux renommer les var dans list_niv_geo.rds
          pull(1)
      }
      
      global$connect_tbls <- local$connect_tbls
      global$valider <- input$valider
      
    }, ignoreNULL = TRUE, ignoreInit = TRUE)
    
  })
}

## To be copied in the UI
# mod_pour_tous_select_ui("pour_tous_select_ui_1")

## To be copied in the server
# mod_pour_tous_select_server("pour_tous_select_ui_1")
