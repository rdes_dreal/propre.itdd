#' pour_tous_carto UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#'
#' @importFrom shiny NS tagList 
#' @importFrom stats rnorm 
#' @importFrom future future 
#' @importFrom promises %...>% 
mod_pour_tous_carto_ui <- function(id){
  ns <- NS(id)
  tagList(
    uiOutput(ns("cartos"))
  )
}

#' pour_tous_carto Server Functions
#'
#' @noRd 
mod_pour_tous_carto_server <- function(id, global){
  moduleServer( id, function(input, output, session){
    ns <- session$ns
    
    local <- reactiveValues(go = 0)
    cartos <- reactiveVal()
    
    observeEvent(global$valider,{
      req(global$connect_tbls$data_terr)
      req(global$variable)
      req(global$terr_choisi)
      
      
        ## Set up future
      
        dbname <- Sys.getenv("POSTGRES_DB")
        nom_tables <- global$nom_tables
        dataset <- global$dicovar
        niv_geo <- global$niveau
        terr_choisi <- global$terr_choisi
        annee <- global$annee
        variable <- global$variable
        ss_champ <- global$sous_champ
        date_actu_kit <- global$date_actu_kit
        
      future(seed=NULL,{
        
        ## Connect again in the future
        connect <- DBI::dbConnect(
          RPostgres::Postgres(),
          host = Sys.getenv("POSTGRES_HOST"),
          port = Sys.getenv("POSTGRES_PORT"),
          dbname = dbname,
          user = Sys.getenv("POSTGRES_USER"),
          password = Sys.getenv("POSTGRES_PASSWORD")
          )
        
        try_tbl <- purrr::possibly(dplyr::tbl, NULL)
        connect_tbls <- list(
            data_terr = try_tbl(connect, nom_tables$nom_data_terr),
            data_terr_ally = try_tbl(connect, nom_tables$nom_data_terr_ally),
            data_ref = try_tbl(connect, nom_tables$nom_data_ref),
            data_ref_ally = try_tbl(connect, nom_tables$nom_data_ref_ally),
            data_comp = try_tbl(connect, nom_tables$nom_data_comp),
            data_supra1 = try_tbl(connect, nom_tables$nom_data_supra1),
            data_supra2 = try_tbl(connect, nom_tables$nom_data_supra2),
            data_infra1 = try_tbl(connect, nom_tables$nom_data_infra1),
            data_infra2 = try_tbl(connect, nom_tables$nom_data_infra2),
            data_supra_bar = try_tbl(connect, nom_tables$nom_data_supra_bar)
          )
        
        
        propre.itdd::get_map_one_year(
            dataset = dataset,
            niv_geo= niv_geo,
            data_terr= connect_tbls$data_terr,
            data_ref= connect_tbls$data_ref,
            data_infra1= connect_tbls$data_infra1,
            data_infra2= connect_tbls$data_infra2,
            terr_choisi= as.character(terr_choisi),
            annee= annee,
            # couleur_odd = global$couleur_odd,
            variable= variable,
            ss_champ= ss_champ,
            date_actu_kit = date_actu_kit)
      }) %...>%
        cartos()
      
      ## Ne pas enlever
      message("Calcul cartes en parallele")
      
      local$cartos <- NULL
    })
    
    observeEvent(cartos(), {
      local$cartos <- cartos()
      local$action <- rnorm(1)
    })

    output$cartos <- renderUI({
      
      local$action
      local$go <- isolate(local$go + 1)
      
      if(local$go != 1 & is.null(local$cartos)){
        
        tagList(
          div(class="wave",
              span(class="dot"),
              span(class="dot"),
              span(class="dot")
          )
        )
        
      }else{
      
      message("cartes_ui")
      if(!inherits(local$cartos, "try-error") & !is.null(local$cartos)) {
        
        plot_list <-  lapply(names(local$cartos), function(x){
            plotOutput(ns(x))
          })
      }else{
        plot_list <- list()
      }
      
      do.call(tagList, plot_list)
      }
    })
    
    observeEvent(local$go,{

      message("cartes_server")
      
      if(inherits(local$cartos, "try-error")){
        local$cartos <- list("error" = "error")
      }
      
        lapply(names(local$cartos), function(x){
          output[[x]] <- renderPlot({
            
            
            
            validate(
              need( local$cartos[[x]] != "error" || is.null(local$cartos[[x]]) , 
                    catalogue_textes$mod_pour_tous_carto_pb)
            )
            
            local$cartos[[x]]
          })
        })
        removeModal()
    }, ignoreInit = TRUE)
  })
}

## To be copied in the UI
# mod_pour_tous_carto_ui("pour_tous_carto_ui_1")

## To be copied in the server
# mod_pour_tous_carto_server("pour_tous_carto_ui_1")
