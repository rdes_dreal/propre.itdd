#' Renvoie la liste des années differentes disponibles 
#' 
#' @param data Dataframe. Base dans laquelle les donnees sont selectionnees
#' @return une liste
#' @importFrom dplyr distinct pull
#' @examples get_annee_dispo(propre.itdd::odd_reg_i024)
#' @export

get_annee_dispo <- function(data){
  # check_cleaning(data)
  data %>%
    distinct(annee) %>%
    pull()
}

#' Renvoie la liste des territoires ayant des donnees pour toutes les annees disponibles
#' 
#' @param data Dataframe. Base dans laquelle les donnees sont selectionnees
#' @return une liste de caractere
#' @importFrom dplyr count filter pull
#' @examples \dontrun{all_year_for_each(propre.itdd::odd_reg_i024)}
#' @export

all_year_for_each <- function(data){
  #check_cleaning(data)
  years <- length(get_annee_dispo(data))
  data %>%
    filter(!is.na(valeur), type_var=="i") %>% 
    count(libgeo, name = "nombre") %>%
    filter(nombre == years) %>%
    pull(libgeo)
}


#' Renvoie les valeurs extremes (max et min) pour une variable choisie 
#' 
#' @param data Dataframe. Base dans laquelle les donnees sont selectionnees
#' @param var Variable. Le nom de la variable sur laquelle trier les donnees
#' @return une liste de numériques
#' @importFrom dplyr distinct filter pull
#' @examples get_value_extreme_ind(propre.itdd::odd_reg_i024, valeur)
#' @export

get_value_extreme_ind <- function(data, var){
  #check_cleaning(data)
  
  list_val <- data %>%
    filter(type_var=="i") %>%
    distinct({{var}}) %>%
    pull()
  
  max_val <- max(list_val, na.rm=TRUE)
  min_val <- min(list_val, na.rm=TRUE)
  
  list_val_extreme <- c(min_val, max_val)

  return(list_val_extreme)
}


#' Renvoie la liste des territoires ayant la valeur min pour une variable choisie 
#' 
#' @param data Dataframe. Base dans laquelle les donnees sont selectionnees
#' @param var Variable. Le nom de la variable sur laquelle trier les donnees
#' @return une liste de caracteres
#' @importFrom dplyr distinct filter pull
#' @examples get_terr_ind_value_min(propre.itdd::odd_reg_i024, valeur)
#' @export

get_terr_ind_value_min <- function(data, var){
  #check_cleaning(data)
  
  list_val_extreme <- get_value_extreme_ind({{data}}, {{var}})
  min_val <- min(list_val_extreme, na.rm=TRUE)
  
  data   %>%
    filter(type_var=="i") %>%
    filter({{var}} == min_val) %>%
    pull(codgeo)
}


#' Renvoie la liste des territoires ayant la valeur max pour une variable choisie 
#' 
#' @param data Dataframe. Base dans laquelle les donnees sont selectionnees
#' @param var Variable. Le nom de la variable sur laquelle trier les donnees
#' @return une liste de caracteres
#' @importFrom dplyr distinct filter pull
#' @examples get_terr_ind_value_max(propre.itdd::odd_reg_i024, valeur)
#' @export

get_terr_ind_value_max <- function(data, var){
  #check_cleaning(data)
  
  list_val_extreme <- get_value_extreme_ind({{data}}, {{var}})
  max_val <- max(list_val_extreme, na.rm=TRUE)
  
  data   %>%
    filter(type_var=="i") %>%
    filter({{var}} == max_val) %>%
    pull(codgeo)
}


#' Selectionne les donnees des territoires ayant les valeurs extremes (max et min) pour une variable choisie 
#' 
#' @param data Dataframe. Base dans laquelle les donnees sont selectionnees
#' @param var Variable. Le nom de la variable sur laquelle trier les donnees
#' @return un dataframe
#' @importFrom dplyr filter distinct
#' @examples get_data_ind_value_extreme(propre.itdd::odd_reg_i024, valeur)
#' @export

get_data_ind_value_extreme <- function(data, var){
  #check_cleaning(data)
  
  list_val_extreme <- get_value_extreme_ind({{data}}, {{var}})
  
  terr_val_extreme <- data   %>%
    filter(type_var=="i") %>%
    filter({{var}} %in% list_val_extreme) 
  
  extreme_moins_5_obs <- terr_val_extreme %>% 
    group_by({{var}}) %>% 
    count({{var}}) %>% 
    filter(n<5) %>% 
    ungroup() %>% 
    pull({{var}})
  
  terr_val_extreme_epure <- terr_val_extreme %>% 
    filter({{var}} %in% extreme_moins_5_obs)
}


#' Renvoie la mediane pour une variable choisie et une année choisie
#' 
#' @param data Dataframe. Base dans laquelle les donnees sont selectionnees
#' @param var Variable. Le nom de la variable sur laquelle trier les donnees
#' @param year Character. L'année pour laquelle les données seront selectionnées
#' @return un numérique
#' @importFrom dplyr summarise filter pull
#' @importFrom stats median
#' @examples get_mediane_ind(propre.itdd::odd_reg_i024, valeur, "2016")
#' @export

get_mediane_ind <- function(data, var, year){
  #check_cleaning(data)

  mediane <- data %>%
    filter(annee == year, type_var=="i")  %>%
    summarise(mediane = median({{var}}, na.rm = TRUE)) %>%
    pull()
}


#' Renvoie la liste des ODD concerné par les indicateurs
#' @param dataset dataframe du dicovar
#' @param indic liste d indicateur type "i024"
#' @importFrom dplyr filter pull distinct mutate arrange
#' @importFrom tidyr pivot_longer
#' @examples 
#' library(readr)
#' dicovar = readr::read_csv(system.file("dico_var_utf8.csv", 
#' package = "propre.itdd"), col_types = cols())
#' get_odd(dicovar,"i024")
#' @export

get_odd <- function(dataset,indic){
  
  nom_odd <- dataset  %>% 
    filter(no_indic %in% indic) %>% 
    pivot_longer(cols=c(odd1,odd2)) %>% 
    arrange(value) %>% 
    filter(!is.na(value)) %>% 
  #  mutate(ODD=paste0("ODD",value)) %>% 
    mutate(ODD=as.character(value)) %>% 
    distinct(ODD) %>% 
    pull(ODD)
}


#' Renvoie la liste des libellés des ODD 
#' @param dataset dataframe du fichier de libellé
#' @param list_odd liste des ODD
#' @importFrom dplyr filter pull distinct 
#' @examples 
#' library(readr)
#' fichier_odd = readr::read_csv(system.file("fichier_odd.csv", 
#' package = "propre.itdd"), col_types = cols())
#' get_odd_lib(fichier_odd,"1")
#' @export

get_odd_lib <- function(dataset, list_odd){

  odd_lib <- dataset  %>% 
    filter(odd %in% list_odd) %>% 
    distinct(lib_odd) %>% 
    pull(lib_odd)
}

#' Renvoie la liste des indicateurs de l'ODD étudié (dans odd1 et odd2)
#' @param dataset dataframe du dicovar
#' @param var Character. Code de la variable choisie
#' @importFrom dplyr filter pull distinct mutate
#' @importFrom stringr str_detect
#' @examples 
#' library(readr)
#' dicovar = readr::read_csv(system.file("dico_var_utf8.csv", 
#' package = "propre.itdd"), col_types = cols())
#' get_ind_odd(dicovar,"ODD4")
#' @export

get_ind_odd <- function(dataset,var){
  
  if (var=="Tous") {
    nom_ind <- dataset %>% 
      filter(stringr::str_detect(type_var,"indicateur"))
  } else {
    nom_ind <- dataset %>% 
    filter(odd1==var | odd2==var & stringr::str_detect(type_var,"indicateur"))
  }
    nom_ind %>% 
    mutate(ind_odd=no_indic) %>% 
    distinct(ind_odd) %>% 
    pull(ind_odd)
}

#' Renvoie la liste des code_lib des indicateurs etudiés
#' @param dataset dataframe du dicovar
#' @param var Character. Code de la variable choisie
#' @importFrom dplyr filter pull distinct mutate
#' @importFrom stringr str_detect
#' @examples 
#' library(readr)
#' dicovar = readr::read_csv(system.file("dico_var_utf8.csv", 
#' package = "propre.itdd"), col_types = cols())
#' get_ind_code_lib(dicovar,c("i024","i011"))
#' @export

get_ind_code_lib <- function(dataset,var){
  
  nom_ind <- dataset  %>% 
    filter(no_indic %in% var & stringr::str_detect(type_var,"indicateur")) %>% 
    mutate(indic_lib=paste(no_indic,lib_var,sep="-")) %>% 
    distinct(indic_lib) %>% 
    pull(indic_lib)
}

#' Renvoie la liste des lib_sous_champ d'une liste d'indicateurs
#' @param dataset dataframe du dicovar
#' @param var Character. Code de l'indicateur choisi
#' @param sschamp Character. liste des sous_champs choisis
#' @importFrom dplyr filter pull distinct arrange
#' @importFrom stringr str_detect
#' @examples 
#' library(readr)
#' dicovar = readr::read_csv(system.file("dico_var_utf8.csv", 
#' package = "propre.itdd"), col_types = cols())
#' get_ind_sschamp_lib(dicovar,c("i024","i011"),"")
#' @export

get_ind_sschamp_lib <- function(dataset,var, sschamp){
  
  nom_ind <- dataset  %>% 
    filter(no_indic %in% var & stringr::str_detect(type_var,"indicateur")) %>% 
    filter(sous_champ %in% sschamp) %>% 
    arrange(sous_champ) %>% 
    distinct(lib_sous_champ) %>% 
    pull(lib_sous_champ)
}


#' Renvoie la variable de l indicateur étudié
#' @param dataset dataframe du dicovar
#' @param ind Character. Code de la variable choisie
#' @return un caractere
#' @importFrom dplyr filter pull distinct
#' @importFrom stringr str_detect
#' @examples 
#' library(readr)
#' dicovar = readr::read_csv(system.file("dico_var_utf8.csv", 
#' package = "propre.itdd"), col_types = cols())
#' get_var_ind(dicovar,"i001")
#' @export

get_var_ind <- function(dataset,ind){
  
  nom_unite <- dataset  %>% 
    filter(no_indic==ind & stringr::str_detect(type_var,"indicateur")) %>% 
    distinct(no_indic,variable) %>% 
    pull(variable)
}

#' Renvoie l'unité de la variable étudiée
#' @param dataset dataframe du dicovar
#' @param var Character. Code de la variable choisie
#' @return un caractere
#' @importFrom dplyr filter pull distinct
#' @examples 
#' library(readr)
#' dicovar = readr::read_csv(system.file("dico_var_utf8.csv", 
#' package = "propre.itdd"), col_types = cols())
#' get_unite_ind(dicovar,"part_20_24_sortis_nondip")
#' @export

get_unite_ind <- function(dataset,var){

  nom_unite <- dataset  %>% 
    filter(variable==var) %>% 
    distinct(variable, unite) %>% 
    pull(unite)
}


#' Renvoie la source de la variable étudiée
#' @param dataset dataframe du dicovar
#' @param var Character. Code de la variable choisie
#' @return un caractere
#' @importFrom dplyr filter pull distinct
#' @examples 
#' library(readr)
#' dicovar = readr::read_csv(system.file("dico_var_utf8.csv", 
#' package = "propre.itdd"), col_types = cols())
#' get_source_ind(dicovar,"part_20_24_sortis_nondip")
#' @export

get_source_ind <- function(dataset,var){
  
  nom_source <- dataset  %>% 
    filter(variable==var) %>% 
    distinct(variable, source) %>% 
    pull(source)
}


#' Renvoie le libellé de la variable étudiée
#' @param dataset dataframe du dicovar
#' @param var Character. Code de la variable choisie
#' @return un caractere
#' @importFrom dplyr filter pull distinct
#' @importFrom stringr str_detect
#' @examples 
#' library(readr)
#' dicovar = readr::read_csv(system.file("dico_var_utf8.csv", 
#' package = "propre.itdd"), col_types = cols())
#' get_lib_ind(dicovar,"part_20_24_sortis_nondip")
#' @export

get_lib_ind <- function(dataset,var){
  
  lib_ind <- dataset  %>% 
    filter(variable %in% var & stringr::str_detect(type_var,"indicateur")) %>% 
    distinct(variable, lib_var) %>% 
    pull(lib_var)
}


#' Renvoie la liste des libellés des sous-champs  de la variable étudiée
#' @param dataset dataframe du dicovar
#' @param var Character. Code de la variable choisie
#' @param ss_champ Character. Code des sous-champs
#' @importFrom dplyr filter pull distinct 
#' @importFrom stringr str_to_lower str_detect
#' @examples 
#' library(readr)
#' dicovar = readr::read_csv(system.file("dico_var_utf8.csv", 
#' package = "propre.itdd"), col_types = cols())
#' get_sschamp_lib(dicovar,c("taux_pvt"),c("total","moins30"))
#' @export

get_sschamp_lib <- function(dataset,var, ss_champ){
  
  nom_ind <- dataset  %>% 
    filter(variable %in% var & stringr::str_detect(type_var,"indicateur")) %>% 
    filter(str_to_lower(sous_champ) %in% str_to_lower(ss_champ)) %>% 
    distinct(lib_sous_champ) %>% 
    pull(lib_sous_champ)
}

#' Renvoie le code de l'indicateur correspondant de la variable étudiée
#' @param dataset dataframe du dicovar
#' @param var Character. Code de la variable choisie
#' @return un caractere
#' @importFrom dplyr filter pull distinct
#' @examples 
#' library(readr)
#' dicovar = readr::read_csv(system.file("dico_var_utf8.csv", 
#' package = "propre.itdd"), col_types = cols())
#' get_no_ind(dicovar,"part_20_24_sortis_nondip")
#' @export

get_no_ind <- function(dataset,var){
  
  lib_ind <- dataset %>% 
    filter(variable==var) %>% 
    distinct(variable, no_indic) %>% 
    pull(no_indic)
}


#' Renvoie le code couleur de l'ODD correspondant pour l'affichage de la variable étudiée
#' @param dataset dataframe du dicovar
#' @param var Character. Code de la variable choisie
#' @return un caractere
#' @importFrom dplyr filter pull distinct
#' @importFrom readr read_csv cols
#' @importFrom stringr str_detect
#' @examples 
#' library(readr)
#' dicovar = readr::read_csv(system.file("dico_var_utf8.csv", 
#' package = "propre.itdd"), col_types = cols())
#' get_couleur_ind(dicovar,"part_20_24_sortis_nondip")
#' @export

get_couleur_ind <- function(dataset,var){

  num_odd <- dataset  %>% 
    # filter(variable==var) %>% 
    filter(variable %in% var & stringr::str_detect(type_var,"indicateur")) %>% 
    distinct(variable, odd1) %>% 
    pull(odd1)

  couleur_odd <- readr::read_csv(system.file("fichier_odd.csv", package = "propre.itdd"), col_types = cols())  %>% 
    filter(odd==num_odd) %>% 
    pull(couleur_odd)
}


#' Renvoie le libelle du territoire dans une base de donnees
#' 
#' @param data dataframe. table de donnees géographique
#' @param terr_choisi Character. Code du territoire
#' @return un caractere
#' @importFrom dplyr filter pull distinct
#' @importFrom purrr is_empty
#' @examples get_lib_terr(propre.itdd::odd_reg_i024,"84")
#' @export

get_lib_terr <- function(data,terr_choisi){

  lib_terr_choisi <- data %>% 
    filter(codgeo==terr_choisi) %>% 
    distinct(libgeo) %>% 
    pull(libgeo)
  
  if (is_empty(lib_terr_choisi)){lib_terr_choisi <- ""}
  
  return(lib_terr_choisi)
}


#' modal waiting
#'
#' @return modal dialog
#' @export
#' 
modal_waiting <- function(){
  modalDialog(
    tagList(
      p("Calculs en cours", style = "text-align:center"),
      div(class="wave",
          span(class="dot"),
          span(class="dot"),
          span(class="dot")
      )
    ), footer = NULL)
} 

#' Get variable info
#'
#' @param variable variable
#'
#' @importFrom glue glue
#' @param variable odd variable name
#' @examples 
#' library(readr)
#' dicovar = readr::read_csv(system.file("dico_var_utf8.csv", 
#' package = "propre.itdd"), col_types = cols())
#' get_var_infos(dicovar,"part_20_24_sortis_nondip")
#' @noRd
get_var_infos <- function(dataset,variable) {
  list(
    couleur_odd = get_couleur_ind(dataset,variable),
    nom_unite = get_unite_ind(dataset,variable),
    nom_source = get_source_ind(dataset,variable),
    lib_ind = get_lib_ind(dataset,variable)
  )
}

#' Renvoie la version du package utilisé
#' 
#' @return un caractere
#' @importFrom utils packageVersion
#' @examples get_version_pkg()
#' @export

get_version_pkg <- function(){
  
 version_pkg <- as.character(packageVersion("propre.itdd"))
 return(version_pkg)
  
}

#' Renvoie le texte avec des retours à la ligne si trop long
#' 
#' @param texte Character. chaine de caractère à mettre en forme
#' @param longueur Character. nb de caractere avant le retour à la ligne
#' @param indent Character. indentation après le retour à la ligne
#' @return un caractere
#' @examples wrap_text("texte trop long donc retour à la ligne", 10, 0)
#' @export

wrap_text <- function(texte, longueur=60, indent=0)
{

  spacer = paste(replicate(indent, " "), collapse = "")
  texte_decoupe <- strwrap(texte, width= longueur-indent)

  texte_retour_ligne <- paste(rep(spacer), texte_decoupe, collapse = "\n")
  return(texte_retour_ligne)
  
}


