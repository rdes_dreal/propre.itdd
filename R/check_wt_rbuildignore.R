#' check without copy Rbuildignore
#'
#' @export
#' 
#' @importFrom fs dir_ls dir_create file_copy
#' @importFrom withr with_dir
#' @importFrom rcmdcheck rcmdcheck
#' @importFrom here here
#' @importFrom stringr str_replace_all
#'
#' @examples
#' \dontrun{
#' check_sans_bdd()
#' }
check_sans_bdd <- function(){
  r_build_gnore <- file.path(here::here(), ".Rbuildignore")
  a_enlever <- readLines(con = r_build_gnore) %>% paste(collapse = "|") %>% paste0("(", ., ")") %>% str_replace_all("\\$", "")
  files <- fs::dir_ls(recurse = TRUE, regexp = a_enlever, invert = TRUE, type = "file")
  
  temp_dir <- tempdir()
  dir_pkg <- file.path(temp_dir, "propre.itdd")
  dir.create(path = dir_pkg)
  lapply(files, function(x){
    dir_to_create <- dirname(x) 
    fs::dir_create(file.path(dir_pkg, dir_to_create))
    fs::file_copy(path = file.path(here::here(),x) , file.path(dir_pkg, x), overwrite = TRUE)
  })
  
  withr::with_dir(dir_pkg,{
    rcmdcheck::rcmdcheck()  
  })
  
}

