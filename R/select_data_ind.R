#' Selectionne les indicateurs pour l annee  choisie
#'
#' @param data Dataframe. Base dans laquelle les donnees sont selectionnees
#' @param ss_champ Character. Code du sous-champ représenté
#' @param year Character. Annee a garder
#' @return un dataframe
#' @importFrom dplyr filter mutate if_else
#' @importFrom stringr str_to_lower
#' @examples propre.itdd:::data_ind_for_year(propre.itdd::odd_met_i024, ss_champ="", year="2017")
#' @export

data_ind_for_year <- function(data, ss_champ="", year) {
  
  # check_cleaning(data)
  tab_filtree <- data %>%
    filter(annee == year, type_var == "i") %>% # attention à ne garder que les indicateurs et non les composantes
    mutate(valeur_label = if_else(valeur<1, round(valeur, 2),round(valeur,1))) %>% 
    filter(!is.na(valeur)) 
  
  if (ss_champ!="") {
    tab_filtree <- tab_filtree %>%   
      filter(str_to_lower(sous_champ) == str_to_lower(ss_champ))
  } 
  
  return(tab_filtree)
}

#' Selectionne les indicateurs pour toutes annees disponibles
#'
#' @param data Dataframe. Base dans laquelle les donnees sont selectionnees
#' @param ss_champ Character. Code du sous-champ représenté
#' @return un dataframe
#' @importFrom dplyr filter mutate if_else
#' @importFrom stringr str_to_lower
#' @examples data_ind_all_years(propre.itdd::odd_met_i024, ss_champ="")
#' @export

data_ind_all_years <- function(data, ss_champ="") {
  # check_cleaning(data)
  
  tab_filtree <-  data %>%
    filter(type_var == "i") %>% # attention ne garder que les indicateurs et non les composantes
    mutate(valeur_label = if_else(valeur<1, round(valeur, 2),round(valeur,1)), annee = as.numeric(annee))%>% 
    filter(!is.na(valeur)) 
  
  if (ss_champ!="") {
    tab_filtree <- tab_filtree %>%   
      filter(str_to_lower(sous_champ) == str_to_lower(ss_champ))
  } 
  
  return(tab_filtree)
}

