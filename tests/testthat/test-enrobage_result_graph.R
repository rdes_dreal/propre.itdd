#graph_complet

  a <- graph_complet(1,2)
  
test_that("graph_complet works", {
  expect_equal(a$obj_ggplot2,1)
  expect_equal(a$options,2)
})

#resultat_a_afficher

  b <- resultat_a_afficher(1,2)
  
test_that("resultat_a_afficher works", {
  expect_equal(b$graph_statut,1)
  expect_equal(b$graph_complet,2)
})
