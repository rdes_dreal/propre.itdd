
# propre.itdd 1.3.1

* Mise à jour des programmes d'actualisation de la base de données.
* Mise à jour de la base de données et de la documentation avec la version d'avril 2024 du kit Antidote (modification de i036, i079, i103).

# propre.itdd 1.3.0

* Mise à jour des programmes d'actualisation de la base de données.
* Mise à jour de la base de données et de la documentation avec la version de janvier 2024 du kit Antidote.

# propre.itdd 1.2.0

* correction des données des indicateurs i031, i033 et i088
* remplacement des indicateurs i119a, i119b, i119c par i119
* actualisation du dictionnnaire des variables
* suppression de fichiers non utilisés pour alléger le package

# propre.itdd 1.1.0

* Mise à jour des programmes d'actualisation de la base de données.
* Mise à jour de la base de données avec la version de janvier 2023 du kit Antidote.
* Ajout d'un lien vers l'observatoire des ODD IdF.
* Màj du lien vers le guide d'utilisation mis en ligne sur le site de la DEAL Réunion.
* On n'affiche plus le diagramme en barre pour un DOM si il n'y a aucune donnée pour les DOM.
* Dans les courbes, toutes les années seront visibles sur l'abscisse si il y en a peu (au lieu d'une sur deux).

# propre.itdd 1.0.1

* Suppression de tables .rds inutiles.
* ajout de liens vers DIDO pour le téléchargement des données Antidote.
* Remplacement du guide d'utilisation par le vidéo ODDetT en attendant la parution du guide sur un site accessible sans identification.

# propre.itdd 1.0

* Révision légère des textes et des liens présents dans les onglets d'explications.
* Ajustement de la taille des graphiques pour améliorer la lecture.
* Affichage d'un message si il n'y a aucune donnée à afficher plutot qu'un graphique vide.

# propre.itdd 0.3.5

* Dans l'historique et les barres, suppression des niveaux supra si l'indicateur est une quantité. Les comparaisons ne sont pas pertinentes et cela rend le graphique plus lisible.
* Suppression de l'échelle dans les cartes contenant que les DOM car elle est érronée.
* Ajout d'un guide d'utilisation accessible depuis l'accueil.

# propre.itdd 0.3.4

* Actualisation de la version de COGiter pour améliorer l'affichage des DOM dans les cartes
* Surppression de fonctions créées dans ce package car elles sont maintenant présentes dans COGiter

# propre.itdd 0.3.3

* Améliorations de la mise en forme des graphiques:
  - titres en gras
  - échelle en format scientifique 
  - dégradés plus foncés dans l'historique et remplacement des labels valeurs par le nom du DOM dans l'histo DOM

# propre.itdd 0.3.2

* Si l'indicateur choisi à un sous-champ "total" alors celui-ci est selectionné par défaut
* Le bouton "Valider" est maintenant bloqué tant que le sous-champ n'est pas calculé
* Gestion d'un cas particulier où les valeurs d'un indicateur sont trop grandes pour être des "integer"

# propre.itdd 0.3.1

* Gestion du cas particulier où les composantes et l'indicateur n'ont pas les mêmes informations dans le dictionnaire
* Ajout d'un trait entre l'historique et les diagrammes en barre spour plus de lisibilité
* Suppression des cartes NULL pour ne pas avoir de blanc entre les images dans l'interface

# propre.itdd 0.3

* Ajout du libellé du département dans les sous-titres des cartes concernées 
* Ajout d'une légende pour les contours
* Création de fonctions pour définir les couleurs des cartes. En cas de valeurs négatives, la couleur complémentaire de la couleur de l'ODD est utilisée
* Améliorations des cartes en aplats de couleurs:
  - seuils arrondis à des valeurs entieres si les données ont une amplitude supérieure à 1
  - affichage des données manquantes en gris
  - affichage systématique du 0 dans les seuils
* Améliorations des cartes en ronds proportionnels:
  - seuils arrondis à des valeurs entieres si les données ont une faible amplitude et sont des nombres
  - remplissage des ronds par une meme couleur sauf en cas de changement de signe dans les données
  - affichage de points pour les valeurs nulles
  

# propre.itdd 0.2.2

* Ajout d'un test pour rendre la création des barres plus robuste, dans le cas où le territoire de comparaison n'a pas de donnée
* Mise à jour du texte de l'accueil et des définitions (ajouts de sigles)
* Mise à jour du dictionnaire de variable après corrections manuelles
* Mise à jour de la base de données de production avec l'ensemble des indicateurs disponibles dans le kit
* Corrections dans les calculs d'indicateurs EPCI et DOM : prise en compte des cas où
  - le calcul est impossible (directement le taux fourni)
  - aucune donnée n'est disponible pour le territoire
  - une composante est calculée en plus de l'indicateur

# propre.itdd 0.2.1

* Mise à jour de la documentation de màj de la BDD de prod
* Corrections pour que la majorité des données EPCI soient calculées. Il reste 8 indicateurs en erreur car les formules fournies dans le kit ne permettent pas le calcul.
* Correction de l'activation du bouton "valider" : il s'active dès que change le territoire même si on ne change pas le nivgeo
* Correction du surlignage des extrêmes dans les barres (si la région a une valeur supérieur à celles des départements)
* Corrections du parametrage de l'axe des ordonnées dans les courbes (si toutes les données sont négatives)
* Amélioration de la mise en forme des textes sur les graphiques:
  - meilleur positionnement des labels de données
  - précision "Sous-champ" dans le titre
  - retour à la ligne quand la source est trop longue
  - retour à la ligne quand l'unité est trop longue
  - pour cela création d'une fonction de retour à la ligne 
  - pour cela création de fonctions pour la source
* Séparation dans un script dédié des fonctions de selection des indicateurs (et mise en cohérence des tests)

# propre.itdd 0.2.0

* Mise à jour de la documentation de màj de la BDD
* Mise à jour du dictionnaire des variables avec la nouvelle version du kit Antidote de mars 2022 (fait en meme temps que la mise à jour de la base)
* Mise à jour de COGiter pour prendre en compte l'actualisation du COG 2021


# propre.itdd 0.1.6

* ajout de future et promises dans les modules d'affichage des représentations graphiques. Cela permet de calculer en parallèle les histogrammes, les courbes et les cartes et donc de raccourcir le temps d'affichage.


# propre.itdd 0.1.5

* légende de l'histogramme: affichage une année sur deux 
* Arrondis à 2 chiffres après la virgule si données <1
* ajustement de la longueur des titres 
* Mise en forme de l'onglet "indicateur" pour que les titres tiennent sur une ligne et améliorer les explications
* Mise en forme des onglets texte pour que cela soit plus lisible
* bandeau supérieur fixe
* ajout d'exceptions dans les tests en cas de données manquantes


# propre.itdd 0.1.4

* Corrections d'erreurs à l'affichage :
  - représentations graphiques vides car 0 obs pour les filtres choisis;
  - régle un problème de surlignage de la région dans les barres des départements;
  - affiche les départements DOM quand on choisit une région DOM;
  - ajout d'une 2eme décimale dans la légende des cartes si les données sont proches.
* décalage du label dans le diagramme en barre si les valeurs sont faibles

# propre.itdd 0.1.3

* Complément du mode d'emploi pour la mise à jour de l'image Docker
* Amélioration du calcul des données EPCI: plus que 12 indicateurs en erreur (mauvaise formule dans le dictionnaire)

# propre.itdd 0.1.2.9000

* Ajout des représentations en ronds proportionnels quand l'indicateur n'est pas une part
* Amélioration du calcul des données EPCI

# propre.itdd 0.1.2

* Renforcement des fonctions de création des graphiques pour prendre en compte les cas où le territoire n'a pas de données (table vide ou toutes à NA) ou que les tables n'ont pas exactement le meme nombre de variable (normalement pas possible)
* Correction de la prise en compte du sous-champ dans l'interface et les fonctions
* Ajout d'un blocage de "Valider" et ajout d'une notification si le territoire n'a pas de données pour l'indicateur choisi
* Correction de la prise ne compte des départements extremes dans les barres pour éviter les cumuls mais aussi l'affichage de trop de lignes
* Amélioration de la réactivité avec le bouton qui s'active et se désactive.

# propre.itdd 0.1.1

* Documentation pour la maintenance des apps en dev et en prod.

# propre.itdd 0.1.0

* Mise en production de l'application
* Calculs EPCI / DOM inclus dans la base de données
* Intégration Continue et tests unitaires fonctionnels et informatifs
* Vignettes qui compilent sur les données internes dans le CI mais sur la BDD en local
* CI avec {renv} opérationnel avec gestion du cache des packages
* Instruction de mise à jour des données et de l'application présentée dans le README

# propre.itdd 0.0.0.9000

* Added a `NEWS.md` file to track changes to the package.
