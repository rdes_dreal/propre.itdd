## A partir de janvier 2023: Script inutile car les données EPCI sont incluses dans le kit Antidote

library(purrr)
library(stringr)
library(dplyr)
library(tidyr)
library(DBI)

pkgload::load_all()
con <-  get_db_con()


indic_geo <- "com"

tables <- DBI::dbListTables(con)

tables_com <- get_table_by_geo(con, paste0("^all_", indic_geo))
tables_geo_com <- tbl(con, "communes_epci_geo")

correspondance <- readr::read_csv(here::here("inst/correspondance.csv"))

results <- list()


# erreur <-paste0("all_com_",sub("all_epci_", replacement = "", x = names(results)))

# for(i in tables_com["all_com_iXXX"]){  # pour tester les indicateurs en erreur, mettre le nom de la table commune all_com_iXXX
for(i in tables_com){
  
  
  nom <- as.character(i$ops$x)
  nom_base <- str_extract(nom, '[^all_com_].+')
  
  name_epci <- paste0("all_epci_", nom_base)
  
  message("écriture ", name_epci)
  
  safe_calcul <- purrr::possibly(calculs_par_indicateur, "Problème dans le calcul")
  
  data_somme <- data_epci(i, tables_geo_com)
  
  data <- data_somme %>% 
    safe_calcul(correspondance, name_epci, con)
  
  if(length(data) == 1 ){
    if(data == "Problème dans le calcul"){
      message(data)
      try(DBI::dbRemoveTable(con, name_epci), silent = TRUE)
      results[name_epci] <- data
    }
  }
}

readr::write_csv(data.frame(nom = names(results) ,  results = "Problème dans le calcul"), path = "dev/results_epci.csv")