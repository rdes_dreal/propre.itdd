
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Aplication Outil national de visualisation des indicateurs de développement durable

Cette partie est réservée aux développeurs de l’application. Vous
trouverez l’ensemble des informations concernant la maintenance et la
mise en base de ce projet.

Nous travaillons sur une application qui interroge un base de données et
qui est déployée via le datalab de sspcloud avec kubernetes.

## Le schéma

Il y a une plateforme de développement et une plateforme de production.

Les deux plateformes sont relativement similaires et marchent avec
shinyproxy.

Une base de données chacune, une image shinyproxy chacune et une image
de l’app distinct.

### Le rstudio

Un serveur Rstudio est à disposition des développeurs pour manager la
base et aussi développer l’app en dev (voir propre.sspcloud)

## La base de données

La base de données est séparée en deux avec une partie pour le
développement (nom “postgresbdd”) et une partie pour la production (nom
= itddpublic).

Si c’est pour initier la base, veuillez vous référer à la documentation
`documentation/dev-maj_base.Rmd` et
`documentation/traitement_correspondance.Rmd`.

La base de développement comprend l’intégralité des tables possibles. La
base de prod est restreinte et ne comprend qu’une partie copié de la
base de dev. Vous trouverez comment faire pour déplacer les bases de dev
vers prod via `documentation/dev-maj_prod_base.Rmd`.

## Le shinyproxy

Il y a deux SP, un pour le dev et un pour la prod. - SP dev : il utilise
des identifiants de connexion avec user password. L’image de
l’application disponible sur ce SP est
`registry.gitlab.com/rdes_dreal/propre.itdd:latest`. Donc la dernière
image envoyée sur le repo. - SP prod : il est ouvert à tous et permet
d’accèder à limage : `registry.gitlab.com/rdes_dreal/propre.itdd:prod`.

Pour plus d’info sur comment faire pour les mettre à jour, voir
`documentation/MR-dev.rmd` et `documentation/MR-prod.rmd`

Les images et les scripts pour déployer ces SP sont sur
<https://gitlab.com/rdes_dreal/propre.sspcloud>

## Développer l’application

Avec le développement via le rstudio disponible ici
<https://rstudio-oddett-dev.lab.sspcloud.fr/>, vous pouvez développer
l’app dans les bonnes conditions avec la base de données de dev. Ce qui
permet d’ajouter des features et de s’assurer que cela fonctionne sur
les indicateurs et niveaux géographiques.

-   
