---
title: "Untitled"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, eval=FALSE)
```

# Create Dockerfile

- Create a "Dockerfile_golem" with {golem}

```{r}
## If you want to deploy via a generic Dockerfile
# _Create Dockerfile when on the server
golem::add_dockerfile(from = "rocker/geospatial:4.0.1",
                      repos = "https://packagemanager.rstudio.com/all/__linux__/focal/latest",
                      output = "Dockerfile_golem")

```

- Copy lines with all `remotes::install_version()` to the 'Dockerfile', replacing previous ones
- Build the container and send it to Docker Hub

```bash
# 
# Clone R project locally on your computer and build docker
git pull
docker build -t propreitdd .
# Test the image locally
docker run --name propreitdd -p 127.0.0.1:3838:3838 propreitdd &
docker stop propreitdd
docker rm propreitdd
# Tag before sending
docker tag propreitdd thinkr/propreitdd:latest
docker tag propreitdd thinkr/propreitdd:v0.1.0
# Send to Docker hub
docker login --username=<my_username>
docker push thinkr/propreitdd:latest
docker push thinkr/propreitdd:v0.1.0
```

- Launch the application on <https://shinyproxy-cervan.lab.sspcloud.fr/app/dealapp>
  - You may need to give it some time before the Docker image is downloaded but it will be updated automatically
