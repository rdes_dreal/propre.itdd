
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Outil national de visualisation des indicateurs de développement durable

### ODDetT, C’EST QUOI ?

ODDetT (Objectifs de Développement Durable et Territoires) est une
initiative de la DEAL Réunion qui permet, pour tous les territoires
métropolitains et ultramarins, de visualiser et de télécharger des
représentations des indicateurs du développement durable sur votre
territoire et les territoires voisins et de vous comparer à d’autres
niveaux géographiques. Cette application a été développée par le réseau
des directions régionales de l’environnement de l’aménagement et du
logement (DREAL, DEAL et DRIEAT), en partenariat avec l’Insee et le
CGDD. Elle s’appuie sur une palette d’indicateurs éclairant les ODD au
niveau national et territorial (régions, départements, intercommunalités
et communes) transmise par l’Insee et le Service des données et des
études statistiques (SDES) du ministère de la Transition écologique.

### POUR QUOI FAIRE ?

    visualiser et télécharger des représentations des indicateurs du développement durable,
    trouver des repères chiffrés et se comparer à d’autres niveaux géographiques,
    suivre sa contribution à l’atteinte des objectifs de développement durable.

### POUR QUI ?

ODDetT s’adresse à un large public car les ODD concernent par définition
tous les acteurs :

    collectivités territoriales (élus et techniciens),
    établissements publics,
    établissement d’enseignement et de recherche,
    services de l’État,
    entreprises,
    associations,
    citoyens etc…

## Partie développeur

Veuillez visiter le dossier dev !
